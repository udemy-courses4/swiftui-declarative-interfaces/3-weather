//
//  Weather.swift
//  Weather
//
//  Created by Sander de Groot on 16/10/2020.
//

import Foundation

struct WeatherResponse: Decodable {
    var main: Weather
}

struct Weather: Decodable {
    var temp: Double?
    var humidity: Double?
}
