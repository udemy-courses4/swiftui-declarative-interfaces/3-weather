//
//  WeatherApp.swift
//  Weather
//
//  Created by Sander de Groot on 16/10/2020.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
