//
//  ContentView.swift
//  Weather
//
//  Created by Sander de Groot on 16/10/2020.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var viewModel = WeatherViewModel()
    
    var body: some View {
        
        VStack(alignment: .center) {
            
            TextField("Enter city name", text: $viewModel.cityName, onCommit: viewModel.search)
            .multilineTextAlignment(.center)
            .font(.custom("Arial", size: 40))
            .foregroundColor(.white)
            .padding()
            
            Text(viewModel.temperature)
            .font(.custom("Arial", size: 50))
            .foregroundColor(.white)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        .background(Color.green)
        .ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
