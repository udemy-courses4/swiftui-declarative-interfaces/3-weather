//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Sander de Groot on 16/10/2020.
//

import Foundation

final class WeatherViewModel: ObservableObject {
    
    private lazy var service = WeatherService()
    
    @Published var weather: Weather?
    
    var temperature: String {
        if let temp = weather?.temp {
            return String(format: "%.0f°", temp)
        }
        return " "
    }
    
    var humidity: String {
        if let humidity = weather?.humidity {
            return String(format: "%.0f", humidity)
        }
        return " "
    }
    
    var cityName = ""
        
    func search() {
        if let city = cityName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            fetchWeather(for: city)
        }
    }
    
    private func fetchWeather(for city: String) {
        service.getWeather(city: city) { (weather) in
            self.weather = weather
        }
    }
}
