//
//  WeatherService.swift
//  Weather
//
//  Created by Sander de Groot on 16/10/2020.
//

import Foundation

class WeatherService {
    
    func getWeather(city: String, completion: @escaping (Weather?) -> Void) {
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=95826daa242892b5efed487c17661eba&units=metric") else {
            completion(nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            let weather = try? JSONDecoder().decode(WeatherResponse.self, from: data).main
            completion(weather)
        }
        
        task.resume()
    }
}
